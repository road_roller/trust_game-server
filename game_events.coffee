{DECLARE_TIME_LIMIT,CHOOSE_TIME_LIMIT} = require './conf.json'

class WelcomeEvent
	constructor : (player) ->
		@type = "welcome"
		@status = player.status
		@session = player.session
		@is_admin = player.is_admin

class LobbyListEvent
	constructor : (players) ->
		@type = "lobby_list"
		@list = Object.keys(players).map (session) ->
			player = players[session]
			name : player.name
			point : player.point
			reliability : player.calc_reliability()
			status : player.status

class MatchStartEvent
	constructor : (foe,@template_id) ->
		@type = "match_start"
		@foe =
			name : foe.name
			point : foe.point
			reliability : foe.calc_reliability()

class TimedEvent
	constructor : ->
		@timestamp = new Date

class TimeToDeclareEvent extends TimedEvent
	constructor : (@pass_available) ->
		super
		@type = "time_to_declare"
		@rest_time = DECLARE_TIME_LIMIT

class FoeDeclareEvent
	constructor : (@what) ->
		@type = "foe_declare"

class TimeToChooseEvent extends TimedEvent
	constructor : ->
		super
		@type = "time_to_choose"
		@rest_time = CHOOSE_TIME_LIMIT

class MatchResultEvent
	constructor : (@foe_choice) ->
		@type = "match_result"

class TimerSetEvent
	constructor : (@time) ->
		@type = "timer_set"

class GameOverEvent
	constructor : ->
		@type = "game_over"

class YourDeclareEvent
	constructor : (@your_declare) ->
		@type = "your_declare"



# class FoeLeftEvent
# 	xml_obj : ->
# 		{event : _attr : {id : "foe_left"}}

# class MatchStartedEvent
# 	constructor : (my_cards,foe_cards,you_first) ->
# 		@my_cards = my_cards.map (c) ->
# 			card : _attr : c
# 		@foe_cards = foe_cards.map (c) ->
# 			card : _attr : c
# 		@you_first = if you_first then 1 else 0

# 	xml_obj : ->
# 		{event : [{_attr : {id : "match_started", you_first:@you_first}}
# 			, {my_card_set : @my_cards}
# 			, {foe_card_set : @foe_cards}]}

# class FoeSetCard
# 	constructor : (@card_number,@position) ->

# 	xml_obj : ->
# 		{event : _attr : {
# 			id : "foe_set_card"
# 			, card_number : @card_number
# 			, position : @position
# 			}}

# class FoeDrawCard
# 	xml_obj : ->
# 		event : _attr : {
# 			id : "foe_draw_card"
# 		}

# class FoeEndedTurn
# 	xml_obj : ->
# 		event : _attr : {
# 			id : "foe_ended_turn"
# 		}


module.exports =
	WelcomeEvent : WelcomeEvent
	LobbyListEvent : LobbyListEvent
	MatchStartEvent : MatchStartEvent
	TimeToDeclareEvent : TimeToDeclareEvent
	FoeDeclareEvent : FoeDeclareEvent
	TimeToChooseEvent : TimeToChooseEvent
	MatchResultEvent : MatchResultEvent
	TimedEvent : TimedEvent
	TimerSetEvent : TimerSetEvent
	GameOverEvent : GameOverEvent
	YourDeclareEvent : YourDeclareEvent