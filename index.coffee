restify = require 'restify'
crypto = require 'crypto'
randomstring = require 'randomstring'
_ = require 'underscore'
events = require 'events'

E = new events.EventEmitter

{WelcomeEvent,LobbyListEvent,MatchStartEvent,MatchResultEvent,TimerSetEvent,GameOverEvent} = require './game_events'
{Match} = (require './match') E

PING_DEAD_TIME = 150 * 1000
{PORT} = process.env
PORT ?= 8080

server = restify.createServer()
server.on "uncaughtException", (req,res,route,error) ->
	console.error "Uncaught Exception #{error} #{error.stack}"


players = {}
temp_players = {}
matches = {}

waiting_players = []

game_over_tid = null
game_over_time = null


class Player
	constructor : (@name) ->
		@session = randomstring.generate({length:5, charset:"abcdefghijklmnopqrstuvwxyz0123456789"})
		@status = "lobby"
		@point = 0
		@num_match_done = 0
		@num_betrayed = 0
		@num_silent = 0
		@match_log = []
		@is_admin = Object.keys(players).length == 0
		@event_queue = []
		@last_poll_time = +new Date
		@E = new events.EventEmitter
		E.on 'player_enter', @on_player_enter.bind(@)
		E.on 'player_leave', @on_player_leave.bind(@)
		E.on 'timer_set', @on_timer_set.bind(@)
		
		@E.on 'matched', @on_matched.bind(@)

	push_event : (ev) ->
		@event_queue.push ev

	calc_reliability : ->
		if (@num_match_done - @num_silent) == 0
			-1
		else
			1 - @num_betrayed / (@num_match_done - @num_silent)

	on_player_enter : (player) ->
		return if player == @
		@event_queue.push new LobbyListEvent(players)

	on_player_leave : (player) ->
		return if player == @
		if @status == "lobby"
			@event_queue.push new LobbyListEvent(players)

	is_matchable : (foe) ->
		#HOTFIX : wait for algorithm
		foe != @ # && @match_log.indexOf(foe) == -1

	on_matched : (foe,match_template_id) ->
		@match_log.push foe
		@status = "matched"
		@event_queue.push new MatchStartEvent(foe,match_template_id)

	on_timer_set : (time) ->
		@event_queue.push new TimerSetEvent(time)

	toJSON : ->
		session : @session
		status : @status
		event_queue : @event_queue
		last_poll_time : @last_poll_time
		match : if match? then match.match_id else null


server.use restify.queryParser()
server.use restify.jsonBodyParser()

server.use (req,res,next) ->
	if req.params.session?
		req.session = req.params.session
		req.player = players[req.session]
		unless req.player?
			req.player = temp_players[req.session]

			unless req.player?
				next new restify.InvalidArgumentError("no such session")
				return
	
	next()


flush_event_queue = (player,res,next) ->
	player.last_poll_time = +new Date

	res.end JSON.stringify(player.event_queue)
	player.event_queue.length = 0
	next()


server.post '/enter', (req,res,next) ->
	{name} = req.body

	req_player = null

	for session, player of players
		if player.name == name
			req_player = player
			break

	is_new_player = false
	
	unless req_player?
		req_player = new Player(name)
		is_new_player = true

	console.log "new player to #{req_player.session}"
	players[req_player.session] = req_player

	req_player.push_event new WelcomeEvent(req_player)
	req_player.push_event new LobbyListEvent(players)
	if game_over_time?
		req_player.push_event new TimerSetEvent(Math.floor((game_over_time - (+new Date)) / 1000))
	
	if req_player.status == "matched"
		req_player.match.recover_context req_player

	if is_new_player
		E.emit 'player_enter', req_player

	flush_event_queue req_player, res, next


server.post '/leave', (req,res,next) ->
	{session} = req
	console.log "leave"
	unless session?
		next new restify.InvalidArgumentError("no session")
		return

	console.log "leave player : #{session}"
	player = players[session]
	delete players[session]
	player.E.emit 'leave'
	E.emit 'player_leave'
 
	res.send 200
	next()


server.get '/poll', (req,res,next) ->
	{session,player} = req

	unless session?
		next new restify.InvalidArgumentError("no session")
		return

	flush_event_queue player, res, next


server.post '/play', (req,res,next) ->
	{session,player} = req
	console.log "play #{session}"

	unless session?
		next new restify.InvalidArgumentError("no session")
		return

	# player.E.emit 'set_card', card_number, position
	for waiting_player in waiting_players
		if player.is_matchable waiting_player
			waiting_players.splice(waiting_players.indexOf(waiting_player), 1)
			match = new Match(player, waiting_player)
			matches[match.match_id] = match
			break

	unless player.status == 'matched'
		waiting_players.push player

	flush_event_queue player, res, next


server.post '/stay', (req,res,next) ->
	{session,player} = req
	console.log "stay #{session}"

	unless session?
		next new restify.InvalidArgumentError("no session")
		return

	if player.status == 'lobby'
		idx = waiting_players.indexOf(player)
		if idx >= 0
			waiting_players.splice(idx, 1)

	res.send 200
	next()


server.post '/declare', (req,res,next) ->
	{session,player} = req
	console.log "declare #{session}"

	unless session?
		next new restify.InvalidArgumentError("no session")
		return

	{what} = req.params
	console.log "what = #{what}"
	player.E.emit 'declare', what
	res.send 200
	next()


server.post '/choose', (req,res,next) ->
	{session,player} = req
	console.log "choose #{session}"

	unless session?
		next new restify.InvalidArgumentError("no session")
		return

	{what} = req.params
	console.log "what = #{what}"
	player.E.emit 'choose', what
	res.send 200
	next()


game_over = ->
	console.log "GAME OVER!! RESET ALL"
	for session, player of players
		player.push_event new GameOverEvent

	#reset all
	game_over_tid = null
	game_over_time = null
	temp_players = players
	players = {}
	matches = {}


server.post '/set_timer', (req,res,next) ->
	{session,player} = req
	console.log "set timer #{session}"

	unless session?
		next new restify.InvalidArgumentError("no session")
		return

	unless player.is_admin
		next new restify.UnauthorizedError("admin auth required")
		return

	{time} = req.params
	time = parseInt(time)
	console.log "timer set = #{time}"
	E.emit "timer_set", time
	clearTimeout game_over_tid if game_over_tid?
	game_over_tid = setTimeout game_over, time * 1000
	game_over_time = (+new Date) + time * 1000
	res.send 200
	next()


E.on 'match_end', (match) ->
	{p1,p2,match_id,p1_choice,p2_choice,p1_declare,p2_declare,p1_point,p2_point} = match

	p1.push_event new MatchResultEvent(p2_choice)
	p2.push_event new MatchResultEvent(p1_choice)

	p1.point = Math.max(0, p1.point + p1_point)
	p2.point = Math.max(0, p2.point + p2_point)

	p1.num_match_done++
	console.log "p1_declare (#{p1_declare}) p1_choice(#{p1_choice})"
	if p1_declare != p1_choice
		console.log "p1.betrayed++"
		p1.num_betrayed++

	p2.num_match_done++
	console.log "p2_declare (#{p2_declare}) p2_choice(#{p2_choice})"
	if p2_declare == null
		console.log "p2.silent++"
		p2.num_silent++
	else if p2_declare != p2_choice
		console.log "p2.betrayed++"
		p2.num_betrayed++

	p1.status = p2.status = "lobby"
	p1.match = p1.match = null

	delete matches[match_id]

	E.emit 'refresh_player_list'

E.on 'refresh_player_list', ->
	for session, player of players
		player.push_event new LobbyListEvent(players)
	return


server.get '/dump_players', (req,res,next) ->
	res.send players
	next()


server.get '/dump_matches', (req,res,next) ->
	xxx = []
	for k, match of matches
		xxx.push "#{k} : #{match.brief_dump()}"
	res.send xxx.join("   ")
	next()


server.post '/repoint_all', (req,res,next) ->
	for session, player of players
		player.point = Math.floor(Math.random() * 10)

	E.emit 'refresh_player_list'

	res.send 200
	next()


# kill_disconnected = ->
# 	nowtime = +new Date
# 	for session, player of players
# 		if player.last_poll_time + PING_DEAD_TIME < nowtime
# 			# too old => kill
# 			player.E.emit 'leave'
# 			delete players[session]

# 	return


# setInterval kill_disconnected, 1000


server.listen PORT, ->
	console.log "#{server.name} listening at #{server.url}"