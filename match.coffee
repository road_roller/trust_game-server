# shuffle = require 'shuffle-array'

{MatchStartEvent,TimeToDeclareEvent,FoeDeclareEvent,TimeToChooseEvent,MatchResultEvent,TimedEvent,YourDeclareEvent} = require './game_events'
{DECLARE_TIME_LIMIT,CHOOSE_TIME_LIMIT} = require './conf.json'

match_id_counter = 0


class MatchTemplate
	constructor : (@all_silent,@all_confess,@one_silent,@one_confess) ->

templates = [
	new MatchTemplate(3, 1, 0, 6) # 죄수의 딜레마
	, new MatchTemplate(1, -3, 0, 6) # 치킨 게임
	, new MatchTemplate(3, 1, -3, 2) # 사슴 사냥
]

timed = (time_limit,on_normal,on_time_limit) ->
	time_limited = false

	tid = setTimeout ->
		time_limited = true
	, time_limit

	(args...) ->
		return on_time_limit() if time_limited
		clearTimeout tid
		on_normal args...

random_declare_choice = (pass_acceptable) ->
	candidates = ["confess", "silent"]
	candidates.push "pass" if pass_acceptable
	candidates[Math.floor(candidates.length * Math.random())]

random_choice = ->
	candidates = ["confess", "silent"]
	candidates[Math.floor(candidates.length * Math.random())]


module.exports = (E) ->
	class Match
		constructor : (p1,p2) ->
			@match_id = match_id_counter++
			console.log "New Match #{@match_id}! #{p1.session} vs #{p2.session}"

			@p1 = p1
			@p2 = p2
			@p1.match = @p2.match = this
			@P = [p1,p2]

			@template_id = Math.floor(3 * Math.random())
			@template = templates[@template_id]

			@P.forEach (p,i) => p.E.emit 'matched', @P[1 - i], @template_id

			@p1_event_log = []
			@p2_event_log = []

			setTimeout @match_start.bind(@), 3000

		p1_push_event : (e) ->
			@p1.push_event e
			@p1_event_log.push e

		p2_push_event : (e) ->
			@p2.push_event e
			@p2_event_log.push e

		match_start : ->
			@log "match started"
			@p1_push_event new TimeToDeclareEvent(false)
			@p1.E.once 'declare', timed DECLARE_TIME_LIMIT, @on_p1_declare.bind(@), @on_p1_declare.bind(@, random_declare_choice(false))

		on_p1_declare : (what) ->
			@log "p1 declare #{what}"
			@p1_declare = what
			@p1_push_event new YourDeclareEvent(what)
			@p2_push_event new FoeDeclareEvent(what)
			@p2_push_event new TimeToDeclareEvent(true)
			@p2.E.once 'declare', timed DECLARE_TIME_LIMIT, @on_p2_declare.bind(@), @on_p2_declare.bind(@, random_declare_choice(true))

		on_p2_declare : (what) ->
			@log "p2 declare #{what}"
			@p2_declare = what
			@p2_push_event new YourDeclareEvent(what)
			@p1_push_event new FoeDeclareEvent(what)
			@p1_push_event new TimeToChooseEvent()
			@p2_push_event new TimeToChooseEvent()
			@p1_choice = @p2_choice = null
			@p1.E.once 'choose', timed CHOOSE_TIME_LIMIT, @on_p1_choose.bind(@), @on_p1_choose.bind(@, random_choice())
			@p2.E.once 'choose', timed CHOOSE_TIME_LIMIT, @on_p2_choose.bind(@), @on_p2_choose.bind(@, random_choice())

		on_p1_choose : (what) ->
			@log "p1 choose #{what}"
			@p1_choice = what
			@on_player_choose()

		on_p2_choose : (what) ->
			@log "p2 choose #{what}"
			@p2_choice = what
			@on_player_choose()

		on_player_choose : ->
			return if @p1_choice == null || @p2_choice == null

			@log "all player choose"

			if @p1_choice == @p2_choice
				if @p1_choice == "silent"
					@p1_point = @p2_point = @template.all_silent
				else if @p1_choice == "confess"
					@p1_point = @p2_point = @template.all_confess
				else
					throw new Error("unknown choice #{@p1_choice}")
			else
				if @p1_choice == "confess" && @p2_choice == "silent"
					@p1_point = @template.one_confess
					@p2_point = @template.one_silent
				else if @p1_choice == "silent" && @p2_choice == "confess"
					@p1_point = @template.one_silent
					@p2_point = @template.one_confess
				else
					throw new Error("unknown choice #{@p1_choice} #{@p2_choice}")

			E.emit "match_end", @

		recover_context : (p) ->
			foe = if @p1 == p then @p2 else @p1
			p.push_event new MatchStartEvent(foe, @template_id)
			nowtime = new Date
			event_log = (if @p1 == p then @p1_event_log else @p2_event_log)
			for e, i in event_log
				if e instanceof TimedEvent
					continue if i < event_log.length - 1
					e.rest_time -= ((+nowtime) - (+e.timestamp)) / 1000
					e.timestamp = nowtime
				p.push_event e

			return

		brief_dump : ->
			"p1:#{@p1.session} VS p2:#{@p2.session}"

		log : (x, args...) ->
			console.log "Match #{@match_id} : #{x}", args...



	Match : Match